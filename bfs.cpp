//
// Created by mike on 29.10.15.
//


#include <iostream>
#include <queue>

using namespace std;

void BFS(int st, int n, int mass[1000][1000])
{
    int met[1000];
    queue<int>Q;

    met[st] = 1;
    Q.push(st);

    while (Q.size() > 0)
    {
        int u = Q.front();
        Q.pop();
        cout << u + 1 << " ";

        for (int i = 0; i < n; i++)
        {
            if (mass[u][i] == 1 && met[i] != 1){
                Q.push(i);
                met[i] = 1;
            }

        }
    }
};

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int mass[1000][1000];
    int n;

    cin >> n;


    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> mass[i][j];

    cout << "Answer(one of the ways): ";
    BFS(0, n, mass);

    return 0;
}