#include <iostream>
#include <stack>

using namespace std;

void DFS(int st, int n, int mass[1000][1000])
{
    stack<int> S;
    int met[1000];

    S.push(st);
    met[st] = 1;

    while (S.size() > 0)
    {
        int u = S.top();
        S.pop();

        for (int i = 0; i < n; i++)
            if (mass[u][i] == 1 && met[i] != 1)
            {
                S.push(i);
                met[i] = 1;
            }
        cout << " " << u + 1;
    }
}

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int mass[1000][1000];
    int n;

    cin >> n;

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> mass[i][j];

    cout << "Answer: ";
    DFS(0, n, mass);

    return 0;
}
