//
// Created by mike on 30.10.15.
//

#include <iostream>
#include <queue>

using namespace std;

void BFS_Two_Nodes(int st, int en, int n, int mass[1000][1000])
{
    int ways[1000];

    for (int i = 0; i < n; i++)
        ways[i] = 0;

    queue<int> Q;
    Q.push(st);
    Q.pop();

    ways[st] = 0;

    while (Q.size() > 0)
    {
        int u = Q.front();
        Q.pop();

        for (int i = 0; i < n; i ++)
        {
            if (mass[u][i] == 1 && ways[i] != 1){
                Q.push(i);
                ways[i] = ways[u] + 1;
            }
        }
    }
    cout << "Length of way: " << ways[en];
};

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int mass[1000][1000];
    int n, st, en;

    cin >> st >> en >> n;

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j ++)
            cin >> mass[i][j];

    BFS_Two_Nodes(st, en, n, mass);

    return 0;
}
